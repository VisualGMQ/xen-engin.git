#include "platform/openal/openal_audio_buffer.hpp"

namespace Xen {

OpenALAudioBuffer::OpenALAudioBuffer(uint32_t format, const void* data, uint32_t size, uint32_t freq) {
    ALCall(alGenBuffers(1, &buffer_));
    ALCall(alBufferData(buffer_, format, data, size, freq));
}

int OpenALAudioBuffer::GetFrequency() const {
    int freq;
    ALCall(alGetBufferi(buffer_, AL_FREQUENCY, &freq));
    return freq;
}

int OpenALAudioBuffer::GetChannels() const {
    int c;
    ALCall(alGetBufferi(buffer_, AL_CHANNELS, &c));
    return c;
}

OpenALAudioBuffer::~OpenALAudioBuffer() {
    ALCall(alDeleteBuffers(1, &buffer_));
}

}
