#include "platform/openal/openal_audio_api.hpp"

namespace Xen {

OpenALAudioAPI::OpenALAudioAPI() {
    auto device = alcOpenDevice(nullptr);
    if (device) {
        auto context = alcCreateContext(device, nullptr);
        alcMakeContextCurrent(context);
    } else {
        XEN_CORE_ASSERT(false, "can't create OpenAL Context, make sure you have support audio device");
    }
}

OpenALAudioAPI::~OpenALAudioAPI() {
}

void OpenALAudioAPI::Play(const Ref<AudioSource>& source) {
    const auto openal_source = dynamic_cast<OpenALAudioSource*>(source.get());
    ALCall(alSourcePlay(openal_source->GetSource()));
}

void OpenALAudioAPI::Pause(const Ref<AudioSource>& source) {
    const auto openal_source = dynamic_cast<OpenALAudioSource*>(source.get());
    ALCall(alSourcePause(openal_source->GetSource()));
}

void OpenALAudioAPI::Stop(const Ref<AudioSource>& source)  {
    const auto openal_source = dynamic_cast<OpenALAudioSource*>(source.get());
    ALCall(alSourceStop(openal_source->GetSource()));
}

void OpenALAudioAPI::Rewind(const Ref<AudioSource>& source) {
    const auto openal_source = dynamic_cast<OpenALAudioSource*>(source.get());
    ALCall(alSourceRewind(openal_source->GetSource()));
}

}
