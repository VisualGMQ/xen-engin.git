#include "platform/openal/openal_core.hpp"

namespace Xen {

std::string ALErrorCode2Str(ALenum error) {
    switch (error) {
        case AL_INVALID_NAME: return "INVALID_NAME";
        case AL_INVALID_ENUM: return "INVALID_ENUM";
        case AL_INVALID_VALUE: return "INVALID_VALUE";
        case AL_INVALID_OPERATION: return "INVALID_OPERATION";
        case AL_OUT_OF_MEMORY: return "OUT_OF_MEMORY";
        default: return std::to_string(error);
    }
}

}
