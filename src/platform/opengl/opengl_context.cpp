#include "platform/opengl/opengl_context.hpp"

namespace Xen {

bool OpenGLContext::is_glew_init = false;

OpenGLContext::OpenGLContext(SDL_Window* window):window_(window) {
    XEN_CORE_ASSERT(window, "window is nullptr");
}

void OpenGLContext::Init() {
    SDL_GL_CreateContext(window_);
    if (!OpenGLContext::is_glew_init) {
        if (glewInit() != GLEW_OK) {
            XEN_CORE_ASSERT(false, "glew init failed");
        } else {
            OpenGLContext::is_glew_init = true;
        }
    }
    GLClearError();
    XEN_CORE_INFO("OpenGL Vendor: {}, Renderer: {}, Version: {}",
                glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));
}

void OpenGLContext::SwapBuffers() {
    SDL_GL_SwapWindow(window_);
}

}
