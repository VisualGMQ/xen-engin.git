#include "platform/sdl_mixer/listener.hpp"

namespace Xen {

void SDLMixerListener::SetGain(float gain) {
    gain_ = gain;
}

float SDLMixerListener::GetGain() const {
    return gain_;
}

void SDLMixerListener::SetPosition(const glm::vec3 &pos) {
    position_ = pos;
}

glm::vec3 SDLMixerListener::GetPosition() const {
    return position_;
}

void SDLMixerListener::SetVelocity(const glm::vec3 &vel) {
    velocity_ = vel;
}

glm::vec3 SDLMixerListener::GetVelocity() const {
    return velocity_;
}

void SDLMixerListener::SetRotation(const glm::vec3 &rot) {
    rotation_ = rot;
}

glm::vec3 SDLMixerListener::GetRotation() const {
    return rotation_;
}

};
