#include "platform/sdl_mixer/buffer.hpp"

namespace Xen {

static int channel = 0;
constexpr int MaxChannelNum = MIX_CHANNELS;

SDLMixerBuffer::SDLMixerBuffer(const std::string& filename) {
    chunk_ = Mix_LoadWAV(filename.c_str());
    if (!chunk_) {
        XEN_CORE_ASSERT(false, "music " + filename + "can't load");
    }
    channels_ = channel++;
    if (channel > MaxChannelNum) {
        channel = 0;
    }
}

SDLMixerBuffer::~SDLMixerBuffer() {
    Mix_FreeChunk(chunk_);
}

int SDLMixerBuffer::GetFrequency() const {
    return UnknownFreq;
}

int SDLMixerBuffer::GetChannels() const {
    return channels_;
}

}
