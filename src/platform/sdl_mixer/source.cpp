#include "platform/sdl_mixer/source.hpp"

namespace Xen {

void SDLMixerAudioSource::SetPosition(const glm::vec3 &pos) {

}

glm::vec3 SDLMixerAudioSource::GetPosition() const {
    return glm::vec3();
}

void SDLMixerAudioSource::SetVolume(float volume) {
    Mix_VolumeChunk(dynamic_cast<SDLMixerBuffer*>(buffer_.get())->GetChunk(), volume * MIX_MAX_VOLUME);
    volume_ = volume;
}

float SDLMixerAudioSource::GetVolume() const {
    return volume_;
}

void SDLMixerAudioSource::Bind(const Ref<AudioBuffer> &buffer) {
    buffer_ = buffer;
}

Ref<AudioBuffer> SDLMixerAudioSource::GetBindedBuffer() const {
    return buffer_;
}

void SDLMixerAudioSource::Unbind() {
    buffer_ = nullptr;
}

void SDLMixerAudioSource::EnableLoop() {
    loop_ = true;
}

void SDLMixerAudioSource::DisableLoop() {
    loop_ = false;
}

bool SDLMixerAudioSource::IsLoop() const {
    return loop_;
}

void SDLMixerAudioSource::SetVelocity(const glm::vec3 &vel) {

}

glm::vec3 SDLMixerAudioSource::GetVelocity() const {
    return glm::vec3();
}

}
