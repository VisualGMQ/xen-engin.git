#include "xen/physical/core.hpp"

namespace Xen::Physical {

void Init() {
    dInitODE();
}

void Shutdown() {
    dCloseODE();
}

}
