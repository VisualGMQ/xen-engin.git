#include "xen/physical/mass.hpp"

namespace Xen::Physical {

        /*******************************
         * Mass
         *******************************/
        Mass::Mass() {
            dMassSetZero(&mass_);
        }


        /*******************************
         * BoxMass
         *******************************/

        BoxMass::BoxMass(float mass, const glm::vec3 &l) {
            Set(mass, l);
        }

        /*******************************
         * SphereMass
         *******************************/

        SphereMass::SphereMass(float mass, float r) {
            Set(mass, r);
        }

    }
