#include "xen/physical/geom.hpp"

namespace Xen::Physical {

    /**************************
     * Geomentry
     **************************/

    Geomentry::~Geomentry() {
        dGeomDestroy(id_);
    }


    /**************************
     * SphereGeomentry
     **************************/

    SphereGeomentry::SphereGeomentry(const Space& space, float r) {
        id_ = dCreateSphere(space.id_, r);
    }


    /**************************
     * BoxGeomentry
     **************************/

    BoxGeomentry::BoxGeomentry(const Space &space, const glm::vec3 &l) {
        id_ = dCreateBox(space.id_, l.x, l.y, l.z);
    }

    glm::vec3 BoxGeomentry::GetL() const {
        dVector3 l;
        dGeomBoxGetLengths(id_, l);
        return glm::vec3(l[0], l[1], l[2]);
    }

    /**************************
     * PlaneGeomentry
     **************************/

    PlaneGeomentry::PlaneGeomentry(const Space& space, const PlaneParams& params) {
        dCreatePlane(space.id_, params.A, params.B, params.C, params.D);
    }

    PlaneParams PlaneGeomentry::GetParams() const {
        dReal p[4];
        dGeomPlaneGetParams(id_, p);
        PlaneParams params;
        params.A = p[0];
        params.B = p[1];
        params.C = p[2];
        params.D = p[3];
        return params;
    }
}