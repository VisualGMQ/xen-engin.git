#include "xen/physical/space.hpp"

#include "xen/physical/geom.hpp"

namespace Xen::Physical {
    Space::Space() {
        id_ = dHashSpaceCreate(0);
    }

    Space::~Space() {
        dSpaceDestroy(id_);
    }

    Ref<SphereGeomentry> Space::CreateShpere(float r) {
        return std::make_shared<SphereGeomentry>(*this, r);
    }

    Ref<BoxGeomentry> Space::CreateBox(const glm::vec3& l) {
        return std::make_shared<BoxGeomentry>(*this, l);
    }

    Ref<PlaneGeomentry> Space::CreatePlane(const PlaneParams& params) {
        return std::make_shared<PlaneGeomentry>(*this, params);
    }

    void Space::Update(NearCollideCB cb, void* data) {
        if (cb) {
            dSpaceCollide(id_, data, cb);
        } else {
            dSpaceCollide(id_, data, defaultCollideCB);
        }
    }


}
