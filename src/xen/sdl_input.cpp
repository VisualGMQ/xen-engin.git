#include "xen/sdl_input.hpp"
#include "xen/application.hpp"

namespace Xen {

bool SDLInput::IsKeyPressedImpl(Keycode key) {
    auto state = SDL_GetKeyboardState(nullptr);
    return state[key];
}

bool SDLInput::IsKeyReleasedImpl(Keycode key) {
    auto state = SDL_GetKeyboardState(nullptr);
    return !state[key];
}

bool SDLInput::IsMouseButtonPressedImpl(MouseButton button) {
    auto button_state = SDL_GetMouseState(nullptr, nullptr);
    switch (button) {
        case MouseButton::LEFT:
            return button_state & SDL_BUTTON(SDL_BUTTON_LEFT);
        case MouseButton::MIDDLE:
            return button_state & SDL_BUTTON(SDL_BUTTON_MIDDLE);
        case MouseButton::RIGHT:
            return button_state & SDL_BUTTON(SDL_BUTTON_RIGHT);
        default:
            return false;
    }
}

bool SDLInput::IsMouseButtonReleasedImpl(MouseButton button) {
    auto button_state = SDL_GetMouseState(nullptr, nullptr);
    switch (button) {
        case MouseButton::LEFT:
            return !(button_state & SDL_BUTTON(SDL_BUTTON_LEFT));
        case MouseButton::MIDDLE:
            return !(button_state & SDL_BUTTON(SDL_BUTTON_MIDDLE));
        case MouseButton::RIGHT:
            return !(button_state & SDL_BUTTON(SDL_BUTTON_RIGHT));
        default:
            return true;
    }
}

int SDLInput::GetMouseXImpl() {
    int x;
    SDL_GetMouseState(&x, nullptr);
    return x;
}

int SDLInput::GetMouseYImpl() {
    int y;
    SDL_GetMouseState(nullptr, &y);
    return y;
}

}
