#include "xen/entry_point.hpp"

int main(int argc, char** argv) {
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_StartTextInput();

    Xen::Log::Init();
    Xen::Log::GetCoreLogger()->info("core log inited");
    Xen::Log::GetClientLogger()->info("client log inited");

    auto app = Xen::CreateApplication();
    app->Run();
    delete app;

    SDL_StopTextInput();
    SDL_Quit();
    return 0;
}
