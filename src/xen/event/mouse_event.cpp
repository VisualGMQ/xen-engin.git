#include "xen/event/mouse_event.hpp"

namespace Xen {

std::string MouseButtonPressedEvent::ToString() const {
    std::stringstream ss;
    ss << "MouseButtonPressedEvent Button ";
    switch(button_) {
        case MouseButton::LEFT:
            ss << "left button";
            break;
        case MouseButton::RIGHT:
            ss << "right button";
            break;
        case MouseButton::MIDDLE:
            ss << "millde button";
            break;
        default:
            ;
    }
    return ss.str();
}

std::string MouseButtonReleasedEvent::ToString() const {
    std::stringstream ss;
    ss << "MouseButtonReleasedEvent Button ";
    switch(button_) {
        case MouseButton::LEFT:
            ss << "left button";
            break;
        case MouseButton::RIGHT:
            ss << "right button";
            break;
        case MouseButton::MIDDLE:
            ss << "millde button";
            break;
        default:
            ;
    }
    return ss.str();
}

std::string MouseMotionEvent::ToString() const {
    std::stringstream ss;
    ss << "MouseMotionEvent (" << x_ << ", " << y_ << " )";
    return ss.str();
}

std::string MouseScrolledEvent::ToString() const {
    std::stringstream ss;
    ss << "MouseScrolledEvent x:" << x_ << ",  y:" << y_;
    return ss.str();
}

}
