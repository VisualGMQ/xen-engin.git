#include "xen/event/key_event.hpp"

namespace Xen {

std::string KeyPressedEvent::ToString() const {
    std::stringstream ss;
    ss << "KeyPressedEvent: " << key_ << "; repeat count: " << repeat_count_;
    return ss.str();
}

std::string KeyReleasedEvent::ToString() const {
    std::stringstream ss;
    ss << "KeyReleasedEvent: " << key_;
    return ss.str();
}

std::string KeyTypedEvent::ToString() const {
    std::stringstream ss;
    ss << "KeyTyped: " << text_;
    return ss.str();
}

}
