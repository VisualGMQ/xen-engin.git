#include "xen/layer_stack.hpp"

namespace Xen {

LayerStack::LayerStack() {
}

LayerStack::~LayerStack() {
    for (Layer* layer: layers_)
        delete layer;
}

void LayerStack::PushLayer(Layer* layer) {
    layers_.emplace(layers_.begin() + layer_insert_idx_, layer);
    layer_insert_idx_++;
}

void LayerStack::PushOverlay(Layer* layer) {
    layers_.emplace_back(layer);
}

void LayerStack::PopLayer(Layer* layer) {
    auto it = std::find(layers_.begin(), layers_.end(), layer);
    if (it != layers_.end()) {
        layers_.erase(it);
        layer_insert_idx_--;
    }
}

void LayerStack::PopOverlay(Layer* layer) {
    auto it = std::find(layers_.begin(), layers_.end(), layer);
    if (it != layers_.end()) {
        layers_.erase(it);
    }
}

}
