#include "xen/renderer/vertex_array.hpp"
#include "xen/renderer/renderer.hpp"
#include "platform/opengl/opengl_vertex_array.hpp"

namespace Xen {

Ref<VertexArray> VertexArray::Create() {
    if (Renderer::GetAPI() == RendererAPI::API::OpenGL) {
        return Ref<OpenGLVertexArray>(new OpenGLVertexArray);
    }
    if (Renderer::GetAPI() == RendererAPI::API::None) {
        XEN_CORE_ASSERT(false, "None render API is invalid");
        return nullptr;
    }
    XEN_CORE_ASSERT(false, "Unknown render API");
    return nullptr;

}

}
