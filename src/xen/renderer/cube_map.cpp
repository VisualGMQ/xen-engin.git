#include "xen/renderer/cube_map.hpp"

#include "xen/core.hpp"
#include "xen/renderer/renderer.hpp"
#include "platform/opengl/opengl_cubemap.hpp"

namespace Xen {

Ref<CubeMap> CubeMap::Create() {
    if (Renderer::GetAPI() == RendererAPI::API::OpenGL) {
        return std::make_shared<OpenGLCubeMap>();
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

}
