#include "xen/renderer/orthographic_camera_controller.hpp"

namespace Xen {

OrthoCameraController::OrthoCameraController(float w, float h, float zoom_level)
:size_(w, h), zoom_level_(zoom_level), camera_(-w*zoom_level/2, w*zoom_level/2, h*zoom_level/2, -h*zoom_level/2)
{
}

void OrthoCameraController::OnUpdate(TimeStep ts) {
    if (Input::IsKeyPressed(XEN_W)) {
        position_.y -= ts * translation_speed_;
    }
    if (Input::IsKeyPressed(XEN_S)) {
        position_.y += ts * translation_speed_;
    }
    if (Input::IsKeyPressed(XEN_A)) {
        position_.x += ts * translation_speed_;
    }
    if (Input::IsKeyPressed(XEN_D)) {
        position_.x -= ts * translation_speed_;
    }
    camera_.SetPosition(position_.x, position_.y);
}

void OrthoCameraController::OnEvent(Event &e) {
    EventDispatcher dispatcher(e);
    dispatcher.Dispatch<MouseScrolledEvent>(BIND_EVENT_FN(OrthoCameraController::onMouseScrolled));
}

bool OrthoCameraController::onMouseScrolled(MouseScrolledEvent &event) {
    zoom_level_ -= event.GetYScroll();
    if (zoom_level_ <= 0) {
        zoom_level_ = 1.0;
    }
    camera_.SetProjection(-size_.x*zoom_level_/2, size_.x*zoom_level_/2, size_.y*zoom_level_/2, -size_.y*zoom_level_/2);
    return false;
}

}
