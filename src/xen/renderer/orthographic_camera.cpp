#include "xen/renderer/orthographic_camera.hpp"

namespace Xen {

OrthoCamera::OrthoCamera(float left, float right, float top, float bottom): view_(1.0f) {
    project_ = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
    project_view_ = project_*view_;
}

void OrthoCamera::SetProjection(float left, float right, float top, float bottom) {
    project_ = glm::ortho(left, right, bottom, top, -1.0f, 1.0f);
}

void OrthoCamera::recalculateMatrices() {
    view_ = glm::translate(glm::mat4(1.0f), glm::vec3(position_.x, position_.y, 0.0f))*
            glm::rotate(glm::mat4(1.0f), glm::radians(rotation_), glm::vec3(0, 0, 1));
    project_view_ = project_*view_;
}

}
