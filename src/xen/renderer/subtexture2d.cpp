#include "xen/renderer/subtexture2d.hpp"

namespace Xen {

Ref<SubTexture2D> SubTexture2D::CreateFromRect(const Ref<Texture2D>& texture, const glm::vec2& top_left, const glm::vec2& size) {
    const glm::vec2 lb = { top_left.x/texture->GetWidth(), 1 - (top_left.y + size.y)/texture->GetHeight() };
    const glm::vec2 rt = { (top_left.x + size.x)/texture->GetWidth(), 1 - top_left.y/texture->GetHeight() };
    return std::make_shared<SubTexture2D>(texture, lb, rt);
}

SubTexture2D::SubTexture2D(const Ref<Texture2D> &texture, const glm::vec2 &left_bottom, const glm::vec2 &right_top)
:texture_(texture)
{
    texcoords_.at(0) = {right_top.x, right_top.y};
    texcoords_.at(1) = {left_bottom.x, right_top.y};
    texcoords_.at(2) = {left_bottom.x, left_bottom.y};
    texcoords_.at(3) = {right_top.x, left_bottom.y};
}

}
