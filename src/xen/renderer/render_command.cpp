#include "xen/renderer/render_command.hpp"

#include "platform/opengl/opengl_renderer_api.hpp"

namespace Xen {

RendererAPI* RenderCommand::renderer_api_ = nullptr;

void RenderCommand::Init() {
    renderer_api_->Init();
}

}
