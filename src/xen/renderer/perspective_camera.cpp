#include "xen/renderer/perspective_camera.hpp"

namespace Xen {

PerspectiveCamera::PerspectiveCamera(float fovy, float aspect, float znear, float zfar)
:view_(1.0f)
{
    SetProjection(fovy, aspect, znear, zfar);
    project_view_ = project_ * view_;
}

void PerspectiveCamera::SetProjection(float fovy, float aspect, float znear, float zfar) {
    project_ = glm::perspective(glm::radians(fovy), aspect, znear, zfar);
}

void PerspectiveCamera::recalculateMatrices() {
    glm::vec3 front = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.x), glm::vec3(1, 0, 0)) *
                      glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.y), glm::vec3(0, 1, 0)) *
                      glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.z), glm::vec3(0, 0, 1)) *
                      glm::vec4(0, 0, -1, 1);
    glm::vec3 up = glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.x), glm::vec3(1, 0, 0)) *
                   glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.y), glm::vec3(0, 1, 0)) *
                   glm::rotate(glm::mat4(1.0f), glm::radians(rotation_.z), glm::vec3(0, 0, 1)) *
                   glm::vec4(0, 1, 0, 1);
    view_ = glm::lookAt(position_, position_ + front, up);
    project_view_ = project_*view_;
}

}
