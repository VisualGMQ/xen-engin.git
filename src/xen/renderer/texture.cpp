#include "xen/renderer/texture.hpp"

#include "xen/renderer/renderer.hpp"
#include "platform/opengl/opengl_texture.hpp"

namespace Xen {

Ref<Texture2D> Texture2D::CreateColorTexture(const std::string& path) {
    switch (Renderer::GetAPI()) {
        case RendererAPI::API::OpenGL: return std::make_shared<OpenGLColorTexture2D>(path);
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

Ref<Texture2D> Texture2D::CreateColorTexture(uint32_t w, uint32_t h) {
    switch (Renderer::GetAPI()) {
        case RendererAPI::API::OpenGL: return std::make_shared<OpenGLColorTexture2D>(w, h);
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

Ref<Texture2D> Texture2D::CreateDepthTexture(uint32_t w, uint32_t h) {
    switch (Renderer::GetAPI()) {
        case RendererAPI::API::OpenGL: return std::make_shared<OpenGLDepthTexture2D>(w, h);
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

Ref<Texture2D> Texture2D::CreateDepthStencilTexture(uint32_t w, uint32_t h) {
    switch (Renderer::GetAPI()) {
        case RendererAPI::API::OpenGL: return std::make_shared<OpenGLDepthStencilTexture2D>(w, h);
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

Ref<Texture2D> Texture2DLibrary::Load(const std::string& name, const std::string& filepath) {
    if (textures_.find(name) == textures_.end()) {
        textures_[name] = Texture2D::CreateColorTexture(filepath);
    }
    return textures_[name];
}

Ref<Texture2D> Texture2DLibrary::Get(const std::string& name) const {
    auto it = textures_.find(name);
    if (it == textures_.end())
        return nullptr;
    return it->second;
}

void Texture2DLibrary::Add(const std::string& name, const Ref<Texture2D>& texture) {
    if (textures_.find(name) == textures_.end()) {
        textures_[name] = texture;
    }
}

}
