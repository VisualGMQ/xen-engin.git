#include "xen/renderer/buffer.hpp"

#include "platform/opengl/opengl_buffer.hpp"
#include "xen/renderer/renderer.hpp"

namespace Xen {

Ref<VertexBuffer> VertexBuffer::Create(const void* vertex_data, uint32_t size) {
    if (Renderer::GetAPI() == RendererAPI::API::OpenGL) {
        return Ref<OpenGLVertexBuffer>(new OpenGLVertexBuffer(vertex_data, size));
    }
    if (Renderer::GetAPI() == RendererAPI::API::None) {
        XEN_CORE_ASSERT(false, "None render API is invalid");
        return nullptr;
    }
    XEN_CORE_ASSERT(false, "Unknown render API");
    return nullptr;
}

Ref<VertexBuffer> VertexBuffer::Create(uint32_t size) {
    if (Renderer::GetAPI() == RendererAPI::API::OpenGL) {
        return Ref<OpenGLVertexBuffer>(new OpenGLVertexBuffer(size));
    }
    if (Renderer::GetAPI() == RendererAPI::API::None) {
        XEN_CORE_ASSERT(false, "None render API is invalid");
        return nullptr;
    }
    XEN_CORE_ASSERT(false, "Unknown render API");
    return nullptr;
}

Ref<IndexBuffer> IndexBuffer::Create(const uint32_t *indices, uint32_t count) {
    if (Renderer::GetAPI() == RendererAPI::API::OpenGL) {
        return Ref<OpenGLIndexBuffer>(new OpenGLIndexBuffer(indices, count));
    }
    if (Renderer::GetAPI() == RendererAPI::API::None) {
        XEN_CORE_ASSERT(false, "None render API is invalid");
        return nullptr;
    }
    XEN_CORE_ASSERT(false, "Unknown render API");
    return nullptr;
}

}
