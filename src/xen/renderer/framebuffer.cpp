#include "xen/xen.hpp"
#include "xen/renderer/framebuffer.hpp"
#include "platform/opengl/opengl_framebuffer.hpp"

namespace Xen {

Ref<Framebuffer> Framebuffer::Create(const FrameBufferInfo &info) {
    switch (Renderer::GetAPI()) {
        case RendererAPI::API::OpenGL: return std::make_shared<OpenGLFramebuffer>(info);
    }
    XEN_CORE_ASSERT(false, "Unknown API type");
    return nullptr;
}

}
