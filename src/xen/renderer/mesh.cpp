#include "xen/renderer/mesh.hpp"

namespace Xen {

Ref<Mesh> Mesh::Create(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices) {
    return std::make_shared<Mesh>(vertices, indices);
}

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices) {
    auto vertex_buffer = VertexBuffer::Create((void*)vertices.data(), vertices.size() * sizeof(Vertex));
    BufferLayout layout = {
        {"a_Position", ShaderDataType::Float3},
        {"a_TexCoord", ShaderDataType::Float2},
        {"a_Normal",   ShaderDataType::Float3},
        {"a_Tangent",  ShaderDataType::Float3}
    };
    vertex_buffer->SetLayout(layout);
    auto index_buffer = IndexBuffer::Create(indices.data(), indices.size());

    vertex_array_ = VertexArray::Create();
    vertex_array_->SetIndexBuffer(index_buffer);
    vertex_array_->SetVertexBuffer(vertex_buffer);
}

}
