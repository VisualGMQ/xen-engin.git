#include "xen/audio/audio_buffer.hpp"

#include "platform/openal/openal_audio_buffer.hpp"
#include "xen/audio/audio_master.hpp"
#include "platform/sdl_mixer/buffer.hpp"

namespace Xen {

Ref<AudioBuffer> AudioBuffer::Create(const std::string& filepath) {
    Ref<AudioBuffer> buffer = nullptr;
    switch (AudioMaster::GetAPI()) {
#ifdef ENABLE_OPENAL
        case AudioAPI::OpenAL: {
            SDL_AudioSpec wav_spec;
            Uint32 wav_length;
            Uint8 *wav_buffer;
            if (!SDL_LoadWAV(filepath.c_str(), &wav_spec, &wav_buffer, &wav_length)) {
                XEN_CORE_ASSERT(false, "Could not open " + filepath + ": " + SDL_GetError());
            } else {
                buffer = std::make_shared<OpenALAudioBuffer>(AL_FORMAT_STEREO16, wav_buffer, wav_spec.size, wav_spec.freq);
                SDL_FreeWAV(wav_buffer);
            }
            break;
        }
#endif
#ifdef ENABLE_SDL2_MIXER
        case AudioAPI::SDL2_Mixer: {
            buffer = std::make_shared<SDLMixerBuffer>(filepath);
            break;
        }
#endif
        default:
           XEN_CORE_ASSERT(false, "Unknown audio API type");
    }
    return buffer;
}

}
