#include "xen/audio/audio_command.hpp"

#include "xen/audio/audio_master.hpp"

#include "platform/openal/openal_audio_api.hpp"
#include "platform/sdl_mixer/audio_api.hpp"

namespace Xen {

AudioAPI* AudioCommand::audio_api_ = nullptr;

void AudioCommand::Init() {
    switch (AudioMaster::GetAPI()) {
#ifdef ENABLE_OPENAL
        case AudioAPI::API::OpenAL:
            audio_api_ = new OpenALAudioAPI();
            break;
#endif
#ifdef ENABLE_SDL2_MIXER
        case AudioAPI::API::SDL2_Mixer:
            audio_api_ = new SDLMixerAPI();
            break;
#endif
        default:
            XEN_CORE_ASSERT(false, "Unknown audio API type");
    }
}

void AudioCommand::Shutdown() {
    delete audio_api_;
    audio_api_ = nullptr;
}

}
