#include "xen/audio/audio_source.hpp"

#include "xen/audio/audio_master.hpp"
#include "xen/core.hpp"

#include "platform/openal/openal_audio_source.hpp"
#include "platform/sdl_mixer/source.hpp"

namespace Xen {

Ref<AudioSource> AudioSource::Create() {
    switch (AudioMaster::GetAPI()) {
#ifdef ENABLE_OPENAL
        case AudioAPI::API::OpenAL:
            return std::make_shared<OpenALAudioSource>();
#endif
#ifdef ENABLE_SDL2_MIXER
        case AudioAPI::API::SDL2_Mixer:
            return std::make_shared<SDLMixerAudioSource>();
#endif
    }
    XEN_CORE_ASSERT(false, "Unknown audio API type");
    return nullptr;
}

}
