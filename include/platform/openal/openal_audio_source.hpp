#ifndef XENENGIN_PLATFORM_OPANAL_OPENAL_SOURCE_HPP
#define XENENGIN_PLATFORM_OPANAL_OPENAL_SOURCE_HPP

#include "xen/audio/audio_source.hpp"
#include "openal_pch.hpp"
#include "openal_core.hpp"
#include "openal_audio_buffer.hpp"

namespace Xen {

class OpenALAudioSource: public AudioSource {
public:
    OpenALAudioSource();
    ~OpenALAudioSource();

    void SetPosition(const glm::vec3& pos) override;
    glm::vec3 GetPosition() const override;

    void SetVolume(float volume) override;
    float GetVolume() const override;

    void SetVelocity(const glm::vec3& vel) override;
    glm::vec3 GetVelocity() const override;

    void Bind(const Ref<AudioBuffer>& buffer) override;
    void Unbind() override;
    Ref<AudioBuffer> GetBindedBuffer() const override;

    void EnableLoop() override;
    void DisableLoop() override;
    bool IsLoop() const override;

    inline ALuint GetSource() const { return source_; }

private:
    ALuint source_ = 0;
    Ref<AudioBuffer> buffer_ = nullptr;
};

}

#endif
