#ifndef XENENGIN_PLATFORM_OPENAL_OPENAL_LISTENDER_HPP
#define XENENGIN_PLATFORM_OPENAL_OPENAL_LISTENDER_HPP

#include "xen/audio/audio_listener.hpp"
#include "openal_core.hpp"

namespace Xen {

class OpenALAudioListener: public AudioListener {
public:
    OpenALAudioListener() = default;
    ~OpenALAudioListener() = default;

    void SetGain(float gain) override;
    float GetGain() const override;

    void SetPosition(const glm::vec3& pos) override;
    glm::vec3 GetPosition() const override;
    void SetVelocity(const glm::vec3& vel) override;
    glm::vec3 GetVelocity() const override;
    void SetRotation(const glm::vec3& rot) override;
    glm::vec3 GetRotation() const override;

private:
    glm::vec3 rotation_ = {0, 0, 0};
};

}

#endif
