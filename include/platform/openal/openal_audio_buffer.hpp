#ifndef XENENGIN_PLATFORM_OPENAL_OPENAL_AUDIO_BUFFER_HPP
#define XENENGIN_PLATFORM_OPENAL_OPENAL_AUDIO_BUFFER_HPP

#include "xen/pch.hpp"
#include "xen/audio/audio_buffer.hpp"

#include "openal_core.hpp"

namespace Xen {

class OpenALAudioBuffer: public AudioBuffer {
public:
    OpenALAudioBuffer(uint32_t format, const void* data, uint32_t size, uint32_t freq);
    ~OpenALAudioBuffer();

    int GetFrequency() const override;
    int GetChannels() const override;

    ALuint GetBuffer() const { return buffer_; }

private:
    ALuint buffer_ = 0;
};

}

#endif
