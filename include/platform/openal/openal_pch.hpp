#ifndef XENENGIN_PLATFORM_OPANAL_OPENAL_PCH_HPP
#define XENENGIN_PLATFORM_OPANAL_OPENAL_PCH_HPP

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

#include "xen/log.hpp"
#include "xen/core.hpp"

#endif
