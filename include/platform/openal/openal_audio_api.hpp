#ifndef XENENGIN_PLATFORM_OPENAL_OPENAL_AUDIO_API_HPP
#define XENENGIN_PLATFORM_OPENAL_OPENAL_AUDIO_API_HPP

#include "xen/audio/audio_api.hpp"
#include "xen/audio/audio_source.hpp"
#include "openal_pch.hpp"
#include "openal_core.hpp"
#include "openal_audio_source.hpp"

namespace Xen {

class OpenALAudioAPI: public AudioAPI {
public:
    OpenALAudioAPI();
    ~OpenALAudioAPI();

    void Play(const Ref<AudioSource>& source) override;
    void Pause(const Ref<AudioSource>& source) override;
    void Stop(const Ref<AudioSource>& source) override;
    void Rewind(const Ref<AudioSource>& source) override;
};

}

#endif
