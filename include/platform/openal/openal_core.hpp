#ifndef XENENGIN_PLATFORM_OPENAL_OPENAL_CORE_HPP
#define XENENGIN_PLATFORM_OPENAL_OPENAL_CORE_HPP

#include <string>
#include "openal_pch.hpp"

namespace Xen {

std::string ALErrorCode2Str(ALenum error);

inline void ALClearError() {
    while (alGetError() != AL_NO_ERROR);
}

inline void ALPrintError(const char *function, int line, const char* code) {
    ALenum error;
    while ((error = alGetError()) != AL_NO_ERROR) {
        XEN_CORE_ERROR("[OpenGL Error]({} - {}): {}\n{}\n",
                        function, line, ALErrorCode2Str(error).c_str(), code);
    }
}

#define ALCall(x) \
    do { \
        ALClearError(); \
        x; \
        ALPrintError(__FUNCTION__, __LINE__, #x); \
    } while(0)


}

#endif
