#ifndef XENENGINE_PLATFORM_SDL_MIXER_BUFFER_HPP
#define XENENGINE_PLATFORM_SDL_MIXER_BUFFER_HPP

#include "pch.hpp"
#include "xen/audio/audio_buffer.hpp"

namespace Xen {

class SDLMixerBuffer: public AudioBuffer {
public:
    SDLMixerBuffer(const std::string& filename);
    ~SDLMixerBuffer();

    int GetFrequency() const override;
    int GetChannels() const override;

    Mix_Chunk* GetChunk() const { return chunk_; }

private:
    Mix_Chunk* chunk_ = nullptr;
    int channels_ = 0;
};

}

#endif
