#ifndef XENENGINE_PLATFORM_SDL_MIXER_LISTENER_HPP
#define XENENGINE_PLATFORM_SDL_MIXER_LISTENER_HPP

#include "xen/audio/audio_listener.hpp"

namespace Xen {

class SDLMixerListener: public AudioListener {
public:
    void SetGain(float gain) override;
    float GetGain() const override;
    void SetPosition(const glm::vec3 &pos) override;
    glm::vec3 GetPosition() const override;
    void SetVelocity(const glm::vec3 &vel) override;
    glm::vec3 GetVelocity() const override;
    void SetRotation(const glm::vec3 &rot) override;
    glm::vec3 GetRotation() const override;

private:
    float gain_ = 1;
    glm::vec3 position_ = {0, 0, 0};
    glm::vec3 velocity_ = {0, 0, 0};
    glm::vec3 rotation_ = {0, 0, 0};

};

}

#endif
