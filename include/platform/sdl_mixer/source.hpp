#ifndef XENENGINE_PLATFORM_SDL_MIXER_SOURCE_HPP
#define XENENGINE_PLATFORM_SDL_MIXER_SOURCE_HPP

#include "xen/audio/audio_source.hpp"

#include "buffer.hpp"

namespace Xen {

class SDLMixerAudioSource: public AudioSource {
public:
    SDLMixerAudioSource() = default;
    ~SDLMixerAudioSource() = default;

    void SetPosition(const glm::vec3 &pos) override;
    glm::vec3 GetPosition() const override;
    void SetVolume(float volume) override;
    float GetVolume() const override;
    void Bind(const Ref<AudioBuffer> &buffer) override;
    Ref<AudioBuffer> GetBindedBuffer() const override;
    void Unbind() override;
    void EnableLoop() override;
    void DisableLoop() override;
    bool IsLoop() const override;
    void SetVelocity(const glm::vec3 &vel) override;
    glm::vec3 GetVelocity() const override;

private:
    Ref<AudioBuffer> buffer_ = nullptr;
    bool loop_ = false;
    float volume_ = 1;
};

}

#endif
