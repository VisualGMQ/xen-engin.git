#ifndef XENENGINE_PLATFORM_SDL_MIXER_AUDIO_API_HPP
#define XENENGINE_PLATFORM_SDL_MIXER_AUDIO_API_HPP

#include "xen/audio/audio_api.hpp"
#include "pch.hpp"
#include "buffer.hpp"

namespace Xen {

class SDLMixerAPI: public AudioAPI {
public:
    SDLMixerAPI();
    ~SDLMixerAPI();

    void Play(const Ref<AudioSource> &source) override;
    void Pause(const Ref<AudioSource> &source) override;
    void Stop(const Ref<AudioSource> &source) override;
    void Rewind(const Ref<AudioSource> &source) override;

private:
};

}

#endif
