#ifndef XENENGIN_PLATFORM_OPENGL_OPENGL_CORE_HPP
#define XENENGIN_PLATFORM_OPENGL_OPENGL_CORE_HPP

#include "xen/log.hpp"
#include "opengl_pch.hpp"

#include <string>

namespace Xen {

std::string GLErrorCode2Str(GLenum error);

inline void GLClearError() {
    while (glGetError() != GL_NO_ERROR);
}

inline void GLPrintError(const char *function, int line, const char* code) {
    GLenum error;
    while ((error = glGetError()) != GL_NO_ERROR) {
        XEN_CORE_ERROR("[OpenGL Error]({} - {}): {}\n{}\n",
                        function, line, GLErrorCode2Str(error).c_str(), code);
    }
}

#define GLCall(x) \
    do { \
        GLClearError(); \
        x; \
        GLPrintError(__FUNCTION__, __LINE__, #x); \
    } while(0)

}

#endif
