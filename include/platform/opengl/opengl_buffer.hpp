#ifndef XENENGIN_PLATFORM_OPENGL_BUFFER_HPP
#define XENENGIN_PLATFORM_OPENGL_BUFFER_HPP

#include "xen/renderer/buffer.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLVertexBuffer: public VertexBuffer {
public:
    OpenGLVertexBuffer(const void* vertex_data, uint32_t size);
    OpenGLVertexBuffer(uint32_t size);
    ~OpenGLVertexBuffer();

    void Bind() const override;
    void Unbind() const override;

    void SetData(uint32_t offset, uint32_t size, const void *data) override;

    void SetLayout(const BufferLayout& layout) override;
    const BufferLayout& GetLayout() const override;

private:
    GLuint vertex_buffer_ = 0;
    BufferLayout layout_;
};

class OpenGLIndexBuffer: public IndexBuffer {
public:
    OpenGLIndexBuffer(const uint32_t* indices, uint32_t count);
    ~OpenGLIndexBuffer();

    void Bind() const override;
    void Unbind() const override;

    inline uint32_t GetCount() const override { return count_; }

private:
    GLuint indices_buffer_ = 0;
    uint32_t count_ = 0;
};

}

#endif
