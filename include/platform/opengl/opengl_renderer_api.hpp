#ifndef XENENGIN_PLATFORM_OPENGL_RENDERER_API_HPP
#define XENENGIN_PLATFORM_OPENGL_RENDERER_API_HPP

#include "xen/renderer/renderer_api.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLRendererAPI: public RendererAPI {
public:
    void Init() override;
    void Clear() override;
    void ClearDepthBit() override;
    void ClearColorBit() override;
    void ClearStencilBit() override;
    void SetClearColor(const glm::vec4& color) override;

    void SetViewport(uint32_t x, uint32_t y, uint32_t w, uint32_t h) override;
    glm::vec4 GetViewport() override;

    void EnableCullFace() override;
    void DisableCullFace() override;

    void EnabelDepthTest() override;
    void DisableDepthTest() override;
    bool IsEnabelDepthTest() const override;
    void SetDepthFunc(DepthFunc func) override;

    void DrawIndexed(const std::shared_ptr<VertexArray> &vertex_array) override;
    void DrawArrays(const std::shared_ptr<VertexArray>& vertex_array, uint32_t offset, uint32_t count) override;
};

}

#endif
