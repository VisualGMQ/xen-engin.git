#ifndef XENENGIN_OPENGL_VERTEX_ARRAY_HPP
#define XENENGIN_OPENGL_VERTEX_ARRAY_HPP

#include "xen/renderer/vertex_array.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLVertexArray: public VertexArray {
public:
    static GLenum ShaderDataType2OpenGLType(ShaderDataType type);

    OpenGLVertexArray();
    ~OpenGLVertexArray();

    void SetVertexBuffer(const Ref<VertexBuffer>& buffer) override;
    void SetIndexBuffer(const Ref<IndexBuffer>& buffer) override;

    const Ref<VertexBuffer>& GetVertexBuffer() const override { return vertex_buffer_; }
    const Ref<IndexBuffer>& GetIndexBuffer() const override { return index_buffer_; }

    void Bind() override;
    void Unbind() override;

private:
    Ref<VertexBuffer> vertex_buffer_ = nullptr;
    Ref<IndexBuffer> index_buffer_ = nullptr;

    GLuint vao_ = 0;
};

}

#endif
