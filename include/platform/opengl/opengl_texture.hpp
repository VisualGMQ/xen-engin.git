#ifndef XENENGIN_OPENGL_TEXTURE_HPP
#define XENENGIN_OPENGL_TEXTURE_HPP

#include "xen/renderer/texture.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLFramebuffer;

class OpenGLTexture2DBase: public Texture2D {
public:
    friend class OpenGLFramebuffer;

    virtual ~OpenGLTexture2DBase() = default;

    void Bind(uint32_t slot) const override;
    void Unbind() const override;

    uint32_t GetWidth() const override { return w_; }
    uint32_t GetHeight() const override { return h_; }

protected:
    GLuint tex_ = 0;
    uint32_t w_ = 0;
    uint32_t h_ = 0;
};

class OpenGLColorTexture2D: public OpenGLTexture2DBase {
public:
    OpenGLColorTexture2D(const std::string& path);
    OpenGLColorTexture2D(uint32_t w, uint32_t h);
    ~OpenGLColorTexture2D();

    void SetData(const void* data) override;

private:
    GLenum internal_format_ = GL_RGBA;
    GLenum data_format_ = GL_RGBA;
};

class OpenGLDepthTexture2D: public OpenGLTexture2DBase {
public:
    OpenGLDepthTexture2D(uint32_t w, uint32_t h);
    ~OpenGLDepthTexture2D();

    void SetData(const void* data) override;
};

class OpenGLDepthStencilTexture2D: public OpenGLTexture2DBase {
public:
    OpenGLDepthStencilTexture2D(uint32_t w, uint32_t h);
    ~OpenGLDepthStencilTexture2D();

    void SetData(const void* data) override;
};

}

#endif
