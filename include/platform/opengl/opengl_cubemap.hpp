#ifndef XENENGIN_PLATFORM_OPENGL_CUBEMAP_HPP
#define XENENGIN_PLATFORM_OPENGL_CUBEMAP_HPP

#include "xen/renderer/cube_map.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLCubeMap: public CubeMap {
public:
    OpenGLCubeMap();
    ~OpenGLCubeMap();

    void SetData(CubeMap::TextureIndex idx, const std::string& filepath) override;
    void SetData(CubeMap::TextureIndex idx, const void* data, uint32_t w, uint32_t h, uint32_t internal_format, uint32_t data_format) override;

    void Bind() override;
    void Unbind() override;

private:
    GLuint cubemap_ = 0;
};

}

#endif
