#ifndef CONTEXT_OPENGL_CONTEXT_HPP
#define CONTEXT_OPENGL_CONTEXT_HPP

#include "xen/renderer/graphics_context.hpp"
#include "xen/log.hpp"
#include "xen/window.hpp"
#include "opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLContext: public GraphicsContext {
public:
    OpenGLContext(SDL_Window* window);

    void Init() override;
    void SwapBuffers() override;

    inline SDL_GLContext GetNativeContext() { return SDL_GL_GetCurrentContext(); }

private:
    static bool is_glew_init;
    SDL_Window* window_ = nullptr;
};

}

#endif
