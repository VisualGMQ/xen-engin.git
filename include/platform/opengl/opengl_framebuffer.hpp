#ifndef XENENGIN_XEN_OPENGL_OPENGL_FRAMEBUFFER_HPP
#define XENENGIN_XEN_OPENGL_OPENGL_FRAMEBUFFER_HPP

#include "xen/renderer/framebuffer.hpp"
#include "xen/renderer/texture.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "platform/opengl/opengl_texture.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLFramebuffer: public Framebuffer {
public:
    explicit OpenGLFramebuffer(const FrameBufferInfo& info);
    ~OpenGLFramebuffer();

    void Bind() const override;
    void Unbind() const override;

    void AddColorAttachment(const Ref<Texture2D>& color_attachment) override;
    void AddDepthAttachment(const Ref<Texture2D>& depth_attachment) override;
    void AddDepthStencilAttachment(const Ref<Texture2D>& depth_stencil_attachment) override;
    void NoColorAttachment() override;

    bool IsComplete() const override;

    const FrameBufferInfo &GetInfo() const override { return info_; }


private:
    FrameBufferInfo info_;
    std::vector<Ref<Texture2D>> color_attachments_;
    Ref<Texture2D> depth_attachment_ = nullptr;
    Ref<Texture2D> depth_stencil_attachment_ = nullptr;

    GLuint fbo_ = 0;
};

}

#endif
