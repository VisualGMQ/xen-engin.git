#ifndef XENENGIN_PLATFORM_OPENGL_SHADER_HPP
#define XENENGIN_PLATFORM_OPENGL_SHADER_HPP

#include "xen/renderer/shader.hpp"
#include "platform/opengl/opengl_core.hpp"
#include "opengl_pch.hpp"

namespace Xen {

class OpenGLShader: public Shader {
public:
    OpenGLShader(const std::string& name, const std::string& vertex_source, const std::string& fragment_source);
    ~OpenGLShader();

    const std::string& GetName() const override { return name_; }

    void Bind() const override;
    void Unbind() const override;

    void UniformMat3(const std::string &name, const glm::mat3 &mat3) override;
    void UniformMat4(const std::string& name, const glm::mat4& mat) override;
    void UniformFloat(const std::string& name, float value) override;
    void UniformFloat2(const std::string &name, const glm::vec2 &vec2) override;
    void UniformFloat3(const std::string &name, const glm::vec3 &vec3) override;
    void UniformFloat4(const std::string& name, const glm::vec4& value) override;
    void UniformInt(const std::string& name, int value) override;
    void UniformInt2(const std::string &name, const glm::ivec2&) override;
    void UniformInt3(const std::string &name, const glm::ivec3&) override;
    void UniformInt4(const std::string &name, const glm::ivec4&) override;

private:
    GLuint renderer_id_ = 0;
    std::string name_;

    GLint getUniformLocation(const std::string& name);
};

}

#endif
