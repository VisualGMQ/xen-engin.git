#ifndef XENENGIN_XEN_LAYER_STACK_HPP
#define XENENGIN_XEN_LAYER_STACK_HPP

#include "xen/layer.hpp"

namespace Xen {

class LayerStack {
public:
    LayerStack();
    ~LayerStack();

    void PushLayer(Layer* layer);
    void PushOverlay(Layer* layer);
    void PopLayer(Layer* layer);
    void PopOverlay(Layer* layer);

    std::vector<Layer*>::iterator begin() { return layers_.begin();}
    std::vector<Layer*>::iterator end() { return layers_.end();}

private:
    std::vector<Layer*> layers_;
    unsigned int layer_insert_idx_ = 0;
};

}

#endif
