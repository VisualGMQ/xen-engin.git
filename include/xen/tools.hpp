#ifndef XENENGINE_XEN_TOOLS_HPP
#define XENENGINE_XEN_TOOLS_HPP

template <typename T>
T Clamp(const T& min, const T& max, const T& value) {
    if (value < min)
        return min;
    if (value > max)
        return max;
    return value;
}

#endif
