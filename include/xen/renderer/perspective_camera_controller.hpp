#ifndef XENENGIN_XEN_RENDERER_PERSPECTVE_CAMERA
#define XENENGIN_XEN_RENDERER_PERSPECTVE_CAMERA

#include "xen/pch.hpp"
#include "xen/timestep.hpp"
#include "xen/event/event.hpp"
#include "xen/input.hpp"
#include "xen/event/event.hpp"
#include "xen/event/mouse_event.hpp"
#include "perspective_camera.hpp"

namespace Xen {

class PerspectiveCameraController {
public:
    PerspectiveCameraController(float fovy, float aspect, float znear, float zfar);

    inline const PerspectiveCamera& GetCamera() const { return camera_; }

    void OnUpdate(TimeStep ts);
    void OnEvent(Event& e);

private:
    PerspectiveCamera camera_;

    float speed_ = 5.0;
    glm::vec3 position_ = {0, 0, 0};
    glm::vec3 rotation_ = {0, 0, 0};

    bool onMouseMotion(Xen::MouseMotionEvent& e);
};

}

#endif
