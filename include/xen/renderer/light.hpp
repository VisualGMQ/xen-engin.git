#ifndef XENENGIN_XEN_RENDERER_LIGHT_HPP
#define XENENGIN_XEN_RENDERER_LIGHT_HPP

#include "xen/pch.hpp"

namespace Xen {

struct LightPhongSection {
    glm::vec3 ambient = {0, 0, 0};
    glm::vec3 diffuse = {0, 0, 0};
    glm::vec3 specular = {0, 0, 0};
};

struct DirectLight {
    DirectLight():direction({0, -1, -1}) {}
    DirectLight(const LightPhongSection& phong, const glm::vec3& direction):phong(phong), direction(direction) {}

    LightPhongSection phong;
    glm::vec3 direction;
};

struct DotLight {
    struct Attenuation {
        float constant = 0;
        float linear = 0;
        float quadratic = 0;
    };

    DotLight():position(0, 0, 1) {}
    DotLight(const LightPhongSection& phong, const Attenuation& attenuation, const glm::vec3& position):phong(phong), attenuation(attenuation), position(position) {}

    LightPhongSection phong;
    Attenuation attenuation;
    glm::vec3 position;
};

struct SpotLight {
    struct CutOff {
        float innerCutOff = 0;  // degree
        float outerCutOff = 0;  // degree
    };

    SpotLight():direction(-1, 0, -1) {}
    SpotLight(const LightPhongSection& phong, const CutOff& cutoff, const glm::vec3& position, const glm::vec3& direction)
        :phong(phong), position(position), direction(direction), cutoff(cutoff) {}

    LightPhongSection phong;
    glm::vec3 position;
    glm::vec3 direction;
    CutOff cutoff;
};

}

#endif
