#ifndef XENENGIN_XEN_RENDERER_SUBTEXTURE2D_HPP
#define XENENGIN_XEN_RENDERER_SUBTEXTURE2D_HPP

#include "texture.hpp"

namespace Xen {

class SubTexture2D {
public:
    static Ref<SubTexture2D> CreateFromRect(const Ref<Texture2D>& texture, const glm::vec2& top_left, const glm::vec2& size);

    SubTexture2D(const Ref<Texture2D>& texture, const glm::vec2& left_bottom, const glm::vec2& right_top);
    inline const std::array<glm::vec2, 4>& GetTexCoords() const { return texcoords_; }
    const Ref<Texture> GetTexture() const { return texture_; }

private:
    Ref<Texture2D> texture_;
    std::array<glm::vec2, 4> texcoords_;
};

}

#endif
