#ifndef XENENGIN_XEN_RENDERER_FRAMEBUFFER_HPP
#define XENENGIN_XEN_RENDERER_FRAMEBUFFER_HPP

#include "xen/pch.hpp"
#include "xen/core.hpp"
#include "xen/renderer/texture.hpp"

namespace Xen {

enum class FrameBufferRW {
    READ,
    WRITE,
    READ_WRITE
};

struct FrameBufferInfo {
    FrameBufferRW rw;
};

class Framebuffer {
public:
    static Ref<Framebuffer> Create(const FrameBufferInfo& info);

    virtual ~Framebuffer() = default;

    virtual void Bind() const = 0;
    virtual void Unbind() const = 0;

    virtual void AddColorAttachment(const Ref<Texture2D>& color_attachment) = 0;
    virtual void AddDepthAttachment(const Ref<Texture2D>& depth_attachment) = 0;
    virtual void AddDepthStencilAttachment(const Ref<Texture2D>& depth_stencil_attachment) = 0;

    virtual void NoColorAttachment() = 0;

    virtual bool IsComplete() const = 0;

    virtual const FrameBufferInfo& GetInfo() const = 0;
};

}

#endif
