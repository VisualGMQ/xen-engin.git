#ifndef XENENGIN_XEN_RENDERER_ORTHOGRAPHIC_CAMERA_CONTROLLER_HPP
#define XENENGIN_XEN_RENDERER_ORTHOGRAPHIC_CAMERA_CONTROLLER_HPP

#include "orthographic_camera.hpp"
#include "xen/timestep.hpp"
#include "xen/event/event.hpp"
#include "xen/event/window_event.hpp"
#include "xen/event/mouse_event.hpp"
#include "xen/input.hpp"

namespace Xen {

class OrthoCameraController {
public:
    OrthoCameraController(float w, float h, float zoom_level = 1.0);

    inline glm::vec2 GetSize() const { return size_; }
    inline float GetZoomLevel() const { return zoom_level_; }

    inline const OrthoCamera& GetCamera() const { return camera_; }

    void OnUpdate(TimeStep ts);
    void OnEvent(Event& e);

private:
    glm::vec2 size_;
    float zoom_level_;
    OrthoCamera camera_;

    glm::vec2 position_ = {0, 0};
    float translation_speed_ = 1.0;

    bool onMouseScrolled(MouseScrolledEvent& event);
};

}

#endif
