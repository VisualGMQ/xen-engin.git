#ifndef XENENGIN_XEN_RENDERER_CUBE_MAP_HPP
#define XENENGIN_XEN_RENDERER_CUBE_MAP_HPP

#include "xen/pch.hpp"
#include "xen/core.hpp"

namespace Xen {

class CubeMap {
public:
    enum TextureIndex {
        RIGHT = 0,
        LEFT,
        TOP,
        BOTTOM,
        FRONT,
        BACK,
    };

    static Ref<CubeMap> Create();

    virtual void SetData(TextureIndex idx, const std::string& filepath) = 0;
    virtual void SetData(TextureIndex idx, const void* data, uint32_t w, uint32_t h, uint32_t internal_format, uint32_t data_format) = 0;

    virtual void Bind() = 0;
    virtual void Unbind() = 0;
};

}

#endif
