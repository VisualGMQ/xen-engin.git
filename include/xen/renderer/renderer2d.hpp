#ifndef XENENGIN_RENDERER2D_HPP
#define XENENGIN_RENDERER2D_HPP

#include "xen/pch.hpp"
#include "vertex_array.hpp"
#include "shader.hpp"
#include "orthographic_camera.hpp"
#include "render_command.hpp"
#include "renderer.hpp"
#include "texture.hpp"
#include "subtexture2d.hpp"
#include "xen/config/config.hpp"

namespace Xen {

class Renderer2D {
public:
    static void Init();
    static void Shutdown();

    static void BeginScene(const OrthoCamera&);
    static void EndScene();

    static void DrawRect(const glm::vec2& center, const glm::vec2& size, const glm::vec4& color, float z = 0);
    static void DrawTexture(const glm::vec2& center, const glm::vec2& size, const Ref<Texture2D>& texture, float z = 0);
    static void DrawSubTexture(const glm::vec2& center, const glm::vec2& size, const Ref<SubTexture2D>& texture, float z = 0);
};

}

#endif
