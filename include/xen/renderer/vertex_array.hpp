#ifndef XENENGIN_XEN_RENDERER_VERTEX_ARRAY_HPP
#define XENENGIN_XEN_RENDERER_VERTEX_ARRAY_HPP

#include "xen/pch.hpp"
#include "xen/renderer/buffer.hpp"

namespace Xen {

class VertexArray {
public:
    static Ref<VertexArray> Create();

    virtual ~VertexArray() = default;

    virtual void SetVertexBuffer(const std::shared_ptr<VertexBuffer>& buffer) = 0;
    virtual void SetIndexBuffer(const std::shared_ptr<IndexBuffer>& buffer) = 0;

    virtual const Ref<VertexBuffer>& GetVertexBuffer() const = 0;
    virtual const Ref<IndexBuffer>& GetIndexBuffer() const = 0;

    virtual void Bind() = 0;
    virtual void Unbind() = 0;
};

}

#endif
