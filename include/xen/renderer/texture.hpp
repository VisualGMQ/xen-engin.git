#ifndef XENENGIN_XEN_RENDERER_TEXTURE_HPP
#define XENENGIN_XEN_RENDERER_TEXTURE_HPP

#include "xen/pch.hpp"
#include "xen/core.hpp"
#include <unordered_map>

namespace Xen {

class Texture {
public:
    virtual ~Texture() = default;

    virtual uint32_t GetHeight() const = 0;
    virtual uint32_t GetWidth() const = 0;

    virtual void SetData(const void* data) = 0;

    virtual void Bind(uint32_t slot) const = 0;
    virtual void Unbind() const = 0;
};

class Texture2D: public Texture {
public:
    static Ref<Texture2D> CreateColorTexture(const std::string& path);
    static Ref<Texture2D> CreateColorTexture(uint32_t w, uint32_t h);
    static Ref<Texture2D> CreateDepthTexture(uint32_t w, uint32_t h);
    static Ref<Texture2D> CreateDepthStencilTexture(uint32_t w, uint32_t h);

    virtual ~Texture2D() = default;
};

class Texture2DLibrary {
public:
    Ref<Texture2D> Load(const std::string& name, const std::string& filepath);
    Ref<Texture2D> Get(const std::string& name) const;
    void Add(const std::string& name, const Ref<Texture2D>& texture);
private:
    std::unordered_map<std::string, Ref<Texture2D>> textures_;
};

}

#endif
