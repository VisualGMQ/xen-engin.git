#ifndef XENENGIN_XEN_RENDERER_RENDER_COMMAND_HPP
#define XENENGIN_XEN_RENDERER_RENDER_COMMAND_HPP

#include "renderer_api.hpp"
#include "vertex_array.hpp"

namespace Xen {

class Renderer;

class RenderCommand {
public:
    friend Renderer;

    static void Init();

    inline static void DrawIndexed(const Ref<VertexArray>& vertex_array) { renderer_api_->DrawIndexed(vertex_array); }
    inline static void DrawArrays(const Ref<VertexArray>& vertex_array, uint32_t offset, uint32_t count) { renderer_api_->DrawArrays(vertex_array, offset, count); }

    inline static void Clear() { renderer_api_->Clear(); }
    inline static void ClearDepthBit() { renderer_api_->ClearDepthBit(); }
    inline static void ClearColorBit() { renderer_api_->ClearColorBit(); }
    inline static void ClearStencilBit() { renderer_api_->ClearStencilBit(); }
    inline static void SetClearColor(const glm::vec4& color) { renderer_api_->SetClearColor(color); }

    inline static void SetViewport(uint32_t x, uint32_t y, uint32_t w, uint32_t h) { renderer_api_->SetViewport(x, y, w, h); }
    inline static glm::vec4 GetViewport() { return renderer_api_->GetViewport(); }

    inline static void EnableCullFace() { renderer_api_->EnableCullFace(); }
    inline static void DisableCullFace() { renderer_api_->DisableCullFace(); }
    inline static bool IsEnabelDepthTest() { return renderer_api_->IsEnabelDepthTest(); }

    inline static void EnableDepthTest() { renderer_api_->EnabelDepthTest(); }
    inline static void DisableDepthTest() { renderer_api_->DisableDepthTest(); }
    inline static void SetDepthFunc(DepthFunc func) { renderer_api_->SetDepthFunc(func); }

private:
    static RendererAPI* renderer_api_;
};

}

#endif
