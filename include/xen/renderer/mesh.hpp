#ifndef XENENGIN_XEN_RENDERER_MESH_HPP
#define XENENGIN_XEN_RENDERER_MESH_HPP

#include "xen/renderer/vertex_array.hpp"
#include "xen/renderer/material.hpp"

namespace Xen {

struct Vertex {
    glm::vec3 position;
    glm::vec2 texcoord;
    glm::vec3 normal;
    glm::vec3 tangent;
};

class Mesh {
public:
    friend class Renderer3D;

    static Ref<Mesh> Create(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices);

    Mesh(const std::vector<Vertex>& vertices, const std::vector<uint32_t>& indices);

    inline void SetMaterial(const Ref<Material>& material) { material_ = material; }
    inline Ref<Material> GetMaterial() const { return material_; }

private:
    Ref<VertexArray> vertex_array_ = nullptr;
    Ref<Material> material_ = nullptr;
};

}

#endif
