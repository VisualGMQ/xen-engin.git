#ifndef XENENGIN_XEN_RENDERER_MODEL_HPP
#define XENENGIN_XEN_RENDERER_MODEL_HPP

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "xen/renderer/mesh.hpp"
#include "xen/renderer/texture.hpp"

namespace Xen {

class Model {
public:

    Model(std::vector<Ref<Mesh>>& meshes): meshes_(meshes) {}

    const std::vector<Ref<Mesh>>& GetMeshes() const { return meshes_; }

private:
    std::vector<Ref<Mesh>> meshes_;
};

class ModelLoader {
public:
    inline static Ref<Model> Create(Texture2DLibrary& library, const std::string& filepath) {
        ModelLoader loader(library, filepath);
        return std::make_shared<Model>(loader.meshes_);
    }

    ModelLoader(Texture2DLibrary& library, const std::string& filepath);

private:
    std::vector<Ref<Mesh>> meshes_;
    std::string parent_path_;
    const aiScene* scene_ = nullptr;
    Texture2DLibrary& library_;

    void processNode(const aiNode* node);
    Ref<Mesh> processMesh(const aiMesh* mesh);
    std::vector<Ref<Texture2D>> loadMaterialTextures(aiMaterial* material, aiTextureType type);
};

}

#endif
