#ifndef XENENGIN_XEN_RENDERER_RENDERER_HPP
#define XENENGIN_XEN_RENDERER_RENDERER_HPP

#include "renderer_api.hpp"
#include "render_command.hpp"
#include "vertex_array.hpp"
#include "shader.hpp"
#include "orthographic_camera.hpp"
#include "perspective_camera.hpp"

namespace Xen {

class Renderer {
public:
    inline static RendererAPI::API GetAPI() { return api_; }

    static void OnWindowResize(uint32_t w, uint32_t h);

    static void Init(RendererAPI::API api);
    static void Shutdown();

    static void BeginScene(const OrthoCamera& camera);
    static void BeginScene(const PerspectiveCamera& camera);
    static void EndScene();

    static void Submit(const Ref<VertexArray>& vertex_array, const Ref<Shader>& shader, const glm::mat4& model);

private:
    struct SceneData {
        glm::mat4 project_view;
    };

    static RendererAPI::API api_;
    static SceneData scene_data_;

    static void init();
};

}

#endif
