#ifndef XEN_RENDERER_GRAPHICS_CONTEXT_HPP
#define XEN_RENDERER_GRAPHICS_CONTEXT_HPP

#include "xen/pch.hpp"

namespace Xen {

class GraphicsContext {
public:
    virtual ~GraphicsContext() = default;

    virtual void Init() = 0;
    virtual void SwapBuffers() = 0;

};

}

#endif
