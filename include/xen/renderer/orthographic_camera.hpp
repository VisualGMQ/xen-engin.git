#ifndef XEN_RENDERER_ORTHOGRAPH_CAMERA_HPP
#define XEN_RENDERER_ORTHOGRAPH_CAMERA_HPP

#include "xen/pch.hpp"

namespace Xen {

class OrthoCamera {
public:
    OrthoCamera(float left, float right, float top, float bottom);

    const glm::mat4& GetPorjectMat() const { return project_; }
    const glm::mat4& GetViewMat() const { return view_; }
    const glm::mat4& GetProjectViewMat() const { return project_view_; }

    void SetProjection(float left, float right, float top, float bottom);

    glm::vec2 GetPosition() const { return position_; }
    float GetRotation() const { return rotation_; }
    void SetPosition(float x, float y) {
        position_.x = x;
        position_.y = y;
        recalculateMatrices();
    }
    void SetRotation(float degree) {
        rotation_ = degree;
        recalculateMatrices();
    }

private:
    glm::mat4 project_;
    glm::mat4 view_;
    glm::mat4 project_view_;

    glm::vec2 position_ = {0.0f, 0.0f};
    float rotation_ = 0;

    void recalculateMatrices();
};

}

#endif
