#ifndef XENENGIN_XEN_RENDERER_RENDERER_API_HPP
#define XENENGIN_XEN_RENDERER_RENDERER_API_HPP

#include "xen/pch.hpp"
#include "xen/renderer/vertex_array.hpp"

namespace Xen {

enum class DepthFunc {
    Always,
    Never,
    Less,
    Equal,
    LessEqual,
    Greater,
    NotEqual,
    GeaterEqual
};

class RendererAPI {
public:
    enum class API {
        None = 0,
        OpenGL = 1,
    };

    virtual ~RendererAPI() = default;

    virtual void Init() = 0;
    virtual void Clear() = 0;
    virtual void ClearDepthBit() = 0;
    virtual void ClearColorBit() = 0;
    virtual void ClearStencilBit() = 0;
    virtual void SetClearColor(const glm::vec4&) = 0;

    virtual void EnableCullFace() = 0;
    virtual void DisableCullFace() = 0;

    virtual void EnabelDepthTest() = 0;
    virtual void DisableDepthTest() = 0;
    virtual bool IsEnabelDepthTest() const = 0;
    virtual void SetDepthFunc(DepthFunc func) = 0;

    virtual void SetViewport(uint32_t x, uint32_t y, uint32_t w, uint32_t h) = 0;
    virtual glm::vec4 GetViewport() = 0;

    virtual void DrawIndexed(const std::shared_ptr<VertexArray>& vertex_array) = 0;
    virtual void DrawArrays(const std::shared_ptr<VertexArray>& vertex_array, uint32_t offset, uint32_t count) = 0;
};

}

#endif
