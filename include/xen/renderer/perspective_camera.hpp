#ifndef XENENGIN_XEN_RENDERER_PERSPECTIVE_CAMERA_HPP
#define XENENGIN_XEN_RENDERER_PERSPECTIVE_CAMERA_HPP

#include "xen/pch.hpp"

namespace Xen {

class PerspectiveCamera {
public:
    PerspectiveCamera(float fovy, float aspect, float znear, float zfar);

    const glm::mat4& GetPorjectMat() const { return project_; }
    const glm::mat4& GetViewMat() const { return view_; }
    const glm::mat4& GetProjectViewMat() const { return project_view_; }

    void SetProjection(float fovy, float aspect, float znear, float zfar);
    glm::mat4 GetProjection() const { return project_; }

    glm::vec3 GetPosition() const { return position_; }
    glm::vec3 GetRotation() const { return rotation_; }
    void SetPosition(float x, float y, float z) {
        position_.x = x;
        position_.y = y;
        position_.z = z;
        recalculateMatrices();
    }
    void SetRotation(const glm::vec3& rotation) {
        rotation_.x = glm::radians(rotation.x);
        rotation_.y = glm::radians(rotation.y);
        rotation_.z = glm::radians(rotation.z);
        recalculateMatrices();
    }

private:
    glm::mat4 project_;
    glm::mat4 view_;
    glm::mat4 project_view_;

    glm::vec3 position_ = {0.0f, 0.0f, 0.0f};
    glm::vec3 rotation_ = {0.0f, 0.0f, 0.0f};

    void recalculateMatrices();
};

}

#endif
