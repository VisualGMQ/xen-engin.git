#ifndef XEN_SDL_INPUT_HPP
#define XEN_SDL_INPUT_HPP

#include "xen/input.hpp"

namespace Xen {

class SDLInput: public Input {
protected:
    bool IsKeyPressedImpl(Keycode key) override;
    bool IsKeyReleasedImpl(Keycode key) override;
    bool IsMouseButtonPressedImpl(MouseButton button) override;
    bool IsMouseButtonReleasedImpl(MouseButton button) override;
    int GetMouseXImpl() override;
    int GetMouseYImpl() override;
};

Input* Input::instance_ = new SDLInput;

}

#endif
