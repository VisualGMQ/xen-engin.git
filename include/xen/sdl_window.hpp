#ifndef SDL_WINDOW_HPP
#define SDL_WINDOW_HPP

#include "pch.hpp"
#include "window.hpp"
#include "application.hpp"
#include "log.hpp"
#include "event/key_event.hpp"
#include "event/mouse_event.hpp"
#include "event/window_event.hpp"

#include "platform/opengl/opengl_context.hpp"

namespace Xen {

class SDLWindow: public Window {
 public:
    SDLWindow(const char* title, int w, int h);
    ~SDLWindow();

    Sizeu GetSize() const override;

    void OnUpdate() const override;
    void SetEventCallback(const EventCallbackFn& cb) override { cb_ = cb; }

    void* GetNativeWindow() const override { return (void*)window_; }
    void* GetNativeGlContext() const override { return (void*)(&context_); }

 private:
    void pollEvent() const;

    SDL_Window* window_ = nullptr;
    EventCallbackFn cb_ = nullptr;
    OpenGLContext* context_ = nullptr;
};

}

#endif
