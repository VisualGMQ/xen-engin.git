#ifndef XEN_PCH_HPP
#define XEN_PCH_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <functional>
#include <cassert>
#include <memory>
#include <fstream>
#include <streambuf>
#include <unordered_map>
#include <optional>
#include <variant>
#include <list>
#include <random>

#include <SDL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#endif
