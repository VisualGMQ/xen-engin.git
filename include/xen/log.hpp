#ifndef XEN_LOG_HPP
#define XEN_LOG_HPP

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <vector>
#include <memory>

namespace Xen {

class Log {
 public:
     Log() = default;
     ~Log() = default;

     static void Init();
     inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return client_logger_; }
     inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return core_logger_; }

 private:
     static std::shared_ptr<spdlog::logger> client_logger_;
     static std::shared_ptr<spdlog::logger> core_logger_;
};

#define XEN_CORE_ERROR(...) ::Xen::Log::GetCoreLogger()->error(__VA_ARGS__)
#define XEN_CORE_WARN(...) ::Xen::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define XEN_CORE_DEBUG(...) ::Xen::Log::GetCoreLogger()->debug(__VA_ARGS__)
#define XEN_CORE_TRACE(...) ::Xen::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define XEN_CORE_INFO(...) ::Xen::Log::GetCoreLogger()->info(__VA_ARGS__)

#define XEN_ERROR(...) ::Xen::Log::GetClientLogger()->error(__VA_ARGS__)
#define XEN_WARN(...) ::Xen::Log::GetClientLogger()->warn(__VA_ARGS__)
#define XEN_DEBUG(...) ::Xen::Log::GetClientLogger()->debug(__VA_ARGS__)
#define XEN_TRACE(...) ::Xen::Log::GetClientLogger()->trace(__VA_ARGS__)
#define XEN_INFO(...) ::Xen::Log::GetClientLogger()->info(__VA_ARGS__)

}

#endif
