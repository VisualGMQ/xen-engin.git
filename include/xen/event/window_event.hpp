#ifndef XEN_EVENT_WINDOW_EVENT_HPP
#define XEN_EVENT_WINDOW_EVENT_HPP

#include "xen/event/event.hpp"

namespace Xen {

class WindowResizeEvent: public Event {
 public:
    WindowResizeEvent(unsigned int w, unsigned int h): size_{w, h} {}

    inline Sizeu GetSize() const { return size_; }

    std::string ToString() const override {
        std::stringstream ss;
        ss << "WindowResize: (" << size_.w << ", " << size_.h << ")";
        return ss.str();
    }

    EVENT_CLASS_TYPE(WindowResize)
    EVENT_CLASS_CATEGORY(EventCategoryApplication)

 private:
    Sizeu size_;
};

class WindowCloseEvent: public Event {
 public:

    EVENT_CLASS_TYPE(WindowClose)
    EVENT_CLASS_CATEGORY(EventCategoryApplication)
};

}

#endif
