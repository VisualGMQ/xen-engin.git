#ifndef XENENGIN_EVENT_MOUSE_EVENT_HPP
#define XENENGIN_EVENT_MOUSE_EVENT_HPP

#include "event.hpp"

namespace Xen {

enum class MouseButton {
    LEFT,
    RIGHT,
    MIDDLE
};

class MouseButtonPressedEvent: public Event {
public:
    explicit  MouseButtonPressedEvent(MouseButton button): button_(button) {}
    ~MouseButtonPressedEvent() = default;

    inline MouseButton GetButton() const { return button_; }
    std::string ToString() const override;

    EVENT_CLASS_TYPE(MouseButtonPressed)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryMouseButton)

private:
    MouseButton button_;
};

class MouseButtonReleasedEvent: public Event {
public:
    explicit  MouseButtonReleasedEvent(MouseButton button): button_(button) {}
    ~MouseButtonReleasedEvent() = default;

    inline MouseButton GetButton() const { return button_; }
    std::string ToString() const override;

    EVENT_CLASS_TYPE(MouseButtonReleased)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryMouseButton)

private:
    MouseButton button_;
};

class MouseMotionEvent: public Event {
public:
    MouseMotionEvent(int x, int y, int offset_x, int offset_y): x_(x), y_(y) , offset_x_(offset_x), offset_y_(offset_y) {}
    ~MouseMotionEvent() = default;

    inline int GetX() const { return x_; }
    inline int GetY() const { return y_; }

    inline int GetOffsetX() const { return offset_x_; }
    inline int GetOffsetY() const { return offset_y_; }

    std::string ToString() const override;

    EVENT_CLASS_TYPE(MouseMotion)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryMouse)

private:
    int x_ = 0;
    int y_ = 0;

    int offset_x_ = 0;
    int offset_y_ = 0;
};

class MouseScrolledEvent: public Event {
public:
    MouseScrolledEvent(int x, int y): x_(x), y_(y) {}
    ~MouseScrolledEvent() = default;

    inline int GetXScroll() const { return x_; }
    inline int GetYScroll() const { return y_; }

    std::string ToString() const override;

    EVENT_CLASS_TYPE(MouseScrolled)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryMouse)

private:
    int x_ = 0;
    int y_ = 0;
};

}

#endif
