#ifndef XEN_EVENT_KEY_EVENT_HPP
#define XEN_EVENT_KEY_EVENT_HPP

#include "xen/event/event.hpp"
#include "xen/key_codes.hpp"

namespace Xen {

class KeyPressedEvent: public Event {
 public:
    KeyPressedEvent(Keycode key, int repeat_count): key_(key), repeat_count_(repeat_count) {}
    ~KeyPressedEvent() = default;

    Keycode GetKey() const { return key_; }
    int GetRepeatCount() const { return repeat_count_; }
    std::string ToString() const override;

    EVENT_CLASS_TYPE(KeyPressed)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryKeyboard)

 private:
    Keycode key_ = XEN_UNKNOWN;
    int repeat_count_ = 0;
};

class KeyReleasedEvent: public Event {
public:
    KeyReleasedEvent(Keycode key): key_(key) {}
    ~KeyReleasedEvent() = default;

    inline Keycode GetKey() const { return key_; }
    std::string ToString() const override;

    EVENT_CLASS_TYPE(KeyReleased)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryKeyboard)

private:
    Keycode key_ = XEN_UNKNOWN;
};

class KeyTypedEvent: public Event {
public:
    explicit KeyTypedEvent(char text[32]) {
        strcpy(text_, text);
    }
    ~KeyTypedEvent() = default;

    inline const char* GetText() const { return text_; }

    std::string ToString() const override;

    EVENT_CLASS_TYPE(KeyTyped)
    EVENT_CLASS_CATEGORY(EventCategoryInput|EventCategoryKeyboard)

private:
    char text_[32] = {0};
};

}

#endif
