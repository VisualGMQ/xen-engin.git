#ifndef XEN_APPLICATION_HPP
#define XEN_APPLICATION_HPP

#include "pch.hpp"
#include "log.hpp"
#include "window.hpp"
#include "event/window_event.hpp"
#include "renderer/shader.hpp"
#include "layer_stack.hpp"
#include "xen/imgui/imgui_layer.hpp"
#include "input.hpp"
#include "renderer/buffer.hpp"
#include "renderer/vertex_array.hpp"
#include "renderer/renderer.hpp"
#include "xen/renderer/orthographic_camera.hpp"
#include "timestep.hpp"
#include "physical/core.hpp"

namespace Xen {

class Application {
public:
    static inline Application& Get() { return *instance_; }

    Application();
    virtual ~Application();

    void Exit() { is_running_ = false; }
    void OnEvent(Event& e);
    bool IsRunning() const { return is_running_; }
    void Run();

    void PushLayer(Layer* layer);
    void PushOverlay(Layer* layer);

    Window& GetWindow() { return *window_; }

private:
    std::unique_ptr<Window> window_ = nullptr;
    bool is_running_ = true;

    bool is_minimized_ = false;

    ImGuiLayer* imgui_layer_ = nullptr;

    LayerStack layer_stack_;

    float old_time_ = 0;

    static Application* instance_;

    bool onWindowResize(WindowResizeEvent& event);
    bool onWindowClose(WindowCloseEvent& e);
};

// Will be defined in client
Application* CreateApplication();

}

#endif
