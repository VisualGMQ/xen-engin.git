#ifndef XEN_INPUT_HPP
#define XEN_INPUT_HPP

#include "xen/pch.hpp"
#include "xen/event/key_event.hpp"
#include "xen/event/mouse_event.hpp"

namespace Xen {

class Input {
public:
    static bool IsKeyPressed(Keycode key) { return instance_->IsKeyPressedImpl(key); }
    static bool IsKeyReleased(Keycode key) { return instance_->IsKeyReleasedImpl(key); }

    static bool IsMouseButtonPressed(MouseButton button) { return instance_->IsMouseButtonPressedImpl(button); }
    static bool IsMouseButtonReleased(MouseButton button) { return instance_->IsMouseButtonReleasedImpl(button); }
    static int GetMouseX() { return instance_->GetMouseXImpl(); }
    static int GetMouseY() { return instance_->GetMouseYImpl(); }

    virtual ~Input() = default;

protected:
    virtual bool IsKeyPressedImpl(Keycode key) = 0;
    virtual bool IsKeyReleasedImpl(Keycode key) = 0;

    virtual bool IsMouseButtonPressedImpl(MouseButton button) = 0;
    virtual bool IsMouseButtonReleasedImpl(MouseButton button) = 0;
    virtual int GetMouseXImpl() = 0;
    virtual int GetMouseYImpl() = 0;

private:
    static Input* instance_;
};

}

#endif
