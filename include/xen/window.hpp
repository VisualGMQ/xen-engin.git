#ifndef XEN_WINDOW_HPP
#define XEN_WINDOW_HPP

#include "event/event.hpp"

namespace Xen {

struct WindowProps {
    std::string title;
    unsigned int width;
    unsigned int height;

    WindowProps(const std::string title = "XenEngin", unsigned int width = 800, unsigned int height = 600)
              :title(title), width(width), height(height) {}
};

class Window {
 public:
    using EventCallbackFn = std::function<void(Event&)>;

    virtual ~Window() = default;

    static Window* Create(const WindowProps& props = WindowProps());

    virtual Sizeu GetSize() const = 0;
    virtual void OnUpdate() const = 0;

    virtual void SetEventCallback(const EventCallbackFn& cb) = 0;

    virtual void* GetNativeWindow() const = 0;
    virtual void* GetNativeGlContext() const = 0;
};

}

#endif
