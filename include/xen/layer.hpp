#ifndef XENENGIN_XEN_LAYER_HPP
#define XENENGIN_XEN_LAYER_HPP

#include "xen/pch.hpp"
#include "xen/event/event.hpp"
#include "xen/timestep.hpp"

namespace Xen {

class Layer {
public:
    Layer(const std::string& name = "layer"): debug_name_(name) {}
    virtual ~Layer() {}

    virtual void OnAttach() {}
    virtual void OnDetach() {}
    virtual void OnUpdate(TimeStep ts) {}
    virtual void OnImGuiRender() {}
    virtual void OnEvent(Event& event) {}

    inline const std::string& GetName() const { return debug_name_; }

protected:
    std::string debug_name_;
};

}

#endif
