#ifndef XENENGIN_AUDIO_AUDIO_BUFFER_HPP
#define XENENGIN_AUDIO_AUDIO_BUFFER_HPP

#include "xen/core.hpp"

namespace Xen {

constexpr int UnknownFreq = 0;

class AudioBuffer {
public:
    static Ref<AudioBuffer> Create(const std::string& filepath);

    virtual ~AudioBuffer() = default;

    virtual int GetFrequency() const = 0;
    virtual int GetChannels() const = 0;

private:
};

}

#endif
