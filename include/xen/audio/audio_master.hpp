#ifndef XENENGIN_AUDIO_AUDIO_MASTER_HPP
#define XENENGIN_AUDIO_AUDIO_MASTER_HPP

#include "xen/audio/audio_listener.hpp"
#include "xen/pch.hpp"
#include "audio_api.hpp"
#include "audio_command.hpp"

namespace Xen {

class AudioMaster {
public:
    static void Init(AudioAPI::API api);
    static void Shutdown();

    inline static AudioAPI::API GetAPI() { return api_; }

    static Ref<AudioBuffer> CreateAudioBuffer(const std::string& filepath);
    static Ref<AudioSource> CreateAudioSource();

    inline static void Play(const Ref<AudioSource>& source) { AudioCommand::Play(source); }
    inline static void Pause(const Ref<AudioSource>& source) { AudioCommand::Pause(source); }
    inline static void Stop(const Ref<AudioSource>& source) { AudioCommand::Stop(source); }
    inline static void Rewind(const Ref<AudioSource>& source) { AudioCommand::Rewind(source); }

    inline static Ref<AudioListener> GetListener() { return listener_; }

private:
    static AudioAPI::API api_;
    static Ref<AudioListener> listener_;
};

class AudioLibrary {
public:
    Ref<AudioBuffer> Load(const std::string name, const std::string& filepath);
    Ref<AudioBuffer> Get(const std::string name);
    void Add(const std::string name, const Ref<AudioBuffer>& buffer);

private:
    std::unordered_map<std::string, Ref<AudioBuffer>> audios_;
};

}

#endif
