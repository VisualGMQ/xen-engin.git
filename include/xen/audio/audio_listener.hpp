#ifndef XENENGIN_XEN_AUDIO_AUDIO_LISTENNER_HPP
#define XENENGIN_XEN_AUDIO_AUDIO_LISTENNER_HPP

#include "xen/pch.hpp"

namespace Xen {

class AudioListener {
public:
    virtual ~AudioListener() = default;

    virtual void SetGain(float gain) = 0;
    virtual float GetGain() const = 0;

    virtual void SetPosition(const glm::vec3& pos) = 0;
    virtual glm::vec3 GetPosition() const = 0;

    virtual void SetVelocity(const glm::vec3& vel) = 0;
    virtual glm::vec3 GetVelocity() const = 0;

    virtual void SetRotation(const glm::vec3& rot) = 0;
    virtual glm::vec3 GetRotation() const = 0;

private:
};

}

#endif
