#ifndef XENENGIN_AUDIO_AUDIO_COMMAND_HPP
#define XENENGIN_AUDIO_AUDIO_COMMAND_HPP

#include "xen/audio/audio_api.hpp"
#include "xen/core.hpp"

namespace Xen {

class AudioCommand {
public:
    static void Init();
    static void Shutdown();

    inline static void Play(const Ref<AudioSource>& source) { audio_api_->Play(source); }
    inline static void Pause(const Ref<AudioSource>& source) { audio_api_->Pause(source); }
    inline static void Stop(const Ref<AudioSource>& source) { audio_api_->Stop(source); }
    inline static void Rewind(const Ref<AudioSource>& source) { audio_api_->Rewind(source); }

private:
    static AudioAPI* audio_api_;
};

}

#endif
