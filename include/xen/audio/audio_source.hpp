#ifndef XENENGIN_AUDIO_AUDIO_SOURCE_HPP
#define XENENGIN_AUDIO_AUDIO_SOURCE_HPP

#include "xen/pch.hpp"
#include "xen/core.hpp"
#include "audio_buffer.hpp"

namespace Xen {

class AudioSource {
public:
    static Ref<AudioSource> Create();

    AudioSource() = default;
    virtual ~AudioSource() = default;

    virtual void SetPosition(const glm::vec3& pos) = 0;
    virtual glm::vec3 GetPosition() const = 0;

    virtual void SetVolume(float volume) = 0;
    virtual float GetVolume() const = 0;

    virtual void Bind(const Ref<AudioBuffer>& buffer) = 0;
    virtual Ref<AudioBuffer> GetBindedBuffer() const = 0;
    virtual void Unbind() = 0;

    virtual void EnableLoop() = 0;
    virtual void DisableLoop() = 0;
    virtual bool IsLoop() const = 0;

    virtual void SetVelocity(const glm::vec3& vel) = 0;
    virtual glm::vec3 GetVelocity() const = 0;
};

}

#endif
