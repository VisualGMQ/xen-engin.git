#ifndef XENENGIN_AUDIO_AUDIO_API_HPP
#define XENENGIN_AUDIO_AUDIO_API_HPP

#include "audio_source.hpp"

namespace Xen {

class AudioAPI {
public:
    enum API {
        None = 0,
#ifdef ENABLE_SDL2_MIXER
        SDL2_Mixer,
#endif
#ifdef ENABLE_OPENAL
        OpenAL,
#endif
    };

    virtual ~AudioAPI() = default;

    virtual void Play(const Ref<AudioSource>& source) = 0;
    virtual void Pause(const Ref<AudioSource>& source) = 0;
    virtual void Stop(const Ref<AudioSource>& source) = 0;
    virtual void Rewind(const Ref<AudioSource>& source) = 0;
};

}

#endif
