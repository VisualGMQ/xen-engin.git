#ifndef CORE_HPP
#define CORE_HPP

#include "pch.hpp"
#include "log.hpp"

#define BIT(x) (1 << x)

#ifdef XEN_ENABLE_ASSERT
    #define XEN_CORE_ASSERT(x, ...) do { if(!(x)) { XEN_CORE_ERROR("Assertion Failed!: {0}", __VA_ARGS__); __builtin_debugtrap(); } }while(0)
    #define XEN_CLIENT_ASSERT(x, ...) do { if(!(x)) { XEN_CLIENT_ERROR("Assertion Failed!: {0}", __VA_ARGS__); __builtin_debugtrap(); } }while(0)
#else
    #define XEN_CORE_ASSERT(x, ...)
    #define XEN_CLIENT_ASSERT(x, ...)
#endif

namespace Xen {

template <typename T>
using Ref = std::shared_ptr<T>;

}

#endif
