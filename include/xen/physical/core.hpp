#ifndef XENENGINE_XEN_PHYSICAL_CORE_HPP
#define XENENGINE_XEN_PHYSICAL_CORE_HPP

#include "pch.hpp"

namespace Xen::Physical {

void Init();
void Shutdown();

}

#endif
