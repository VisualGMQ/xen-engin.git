#ifndef XENENGINE_XEN_PHYSICAL_GEOM_HPP
#define XENENGINE_XEN_PHYSICAL_GEOM_HPP

#include "xen/pch.hpp"
#include "pch.hpp"
#include "space.hpp"

namespace Xen::Physical {

        /*********************
         * Geomentry
         *********************/

        class Geomentry {
        public:
            friend class Body;

            virtual ~Geomentry();

            bool operator==(const Geomentry& g) const {
                return IsSameAs(g);
            }

            bool operator!=(const Geomentry& g) const {
                return !IsSameAs(g);
            }

            inline bool IsSameAs(const Geomentry& g) const {
                return id_ == g.id_;
            }

            dGeomID& GetRaw() { return id_; }

        protected:
            dGeomID id_;
        };


        /*********************
         * SphereGeomentry
         *********************/

        class SphereGeomentry: public Geomentry {
        public:
            SphereGeomentry(const Space& space, float r);

            float GetRadius() const { return dGeomSphereGetRadius(id_); }
        };


        /*********************
         * BoxGeomentry
         *********************/

        class BoxGeomentry: public Geomentry {
        public:
            BoxGeomentry(const Space& space, const glm::vec3& l);

            glm::vec3 GetL() const;
        };


        /*********************
         * PlaneGeomentry
         *********************/

         struct PlaneParams {
             float A;
             float B;
             float C;
             float D;
         };

        class PlaneGeomentry: public Geomentry {
        public:
            PlaneGeomentry(const Space& space, const PlaneParams& params);

            PlaneParams GetParams() const;
        };

    }

#endif
