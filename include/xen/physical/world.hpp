#ifndef XENENGINE_XEN_PHYSICAL_WORLD_HPP
#define XENENGINE_XEN_PHYSICAL_WORLD_HPP

#include "pch.hpp"
#include "xen/pch.hpp"
#include "xen/core.hpp"
#include "xen/tools.hpp"
#include "xen/timestep.hpp"

namespace Xen::Physical {

class SphereBody;
class BoxBody;

class World final {
public:
    friend class Body;

    World();
    ~World();

    void SetERP(float value);
    void SetCFM(float value);

    void SetGravity(const glm::vec3& g);
    const glm::vec3 GetGravity() const;

    Ref<SphereBody> CreateSphere(float mass, const glm::vec3& position, float r);
    Ref<BoxBody> CreateBox(float mass, const glm::vec3 &position, const glm::vec3& l);

    inline float GetERP() const { return dWorldGetERP(id_); }
    inline float GetCFM() const { return dWorldGetCFM(id_); }

    inline dWorldID& GetRaw() { return id_; }

    bool operator==(const World& w) const { return IsSameAs(w); }
    bool operator!=(const World& w) const { return !IsSameAs(w); }

    inline bool IsSameAs(const World& w) const { return id_ == w.id_; }

    void Update(TimeStep& ts);

private:
    dWorldID id_;
};

}

#endif
