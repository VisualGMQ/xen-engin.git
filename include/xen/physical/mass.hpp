#ifndef XENENGINE_XEN_PHYSICAL_MASS_HPP
#define XENENGINE_XEN_PHYSICAL_MASS_HPP

#include "xen/pch.hpp"
#include "pch.hpp"

namespace Xen::Physical {

        class Mass {
        public:
            Mass();
            virtual ~Mass() = default;

            inline float GetMass() const { return mass_.mass; }

        protected:
            dMass mass_;
        };

        class BoxMass: public Mass {
        public:
            BoxMass() = default;
            BoxMass(float mass, const glm::vec3& l);

            inline void Set(float mass, const glm::vec3& l) { dMassSetBoxTotal(&mass_, mass, l.x, l.y, l.z); }
        };

        class SphereMass: public Mass {
        public:
            SphereMass() = default;
            SphereMass(float mass, float r);
            inline void Set(float mass, float r) { dMassSetSphereTotal(&mass_, mass, r); }
        };

        // TODO some other geomentry not declare(cyliner, trimesh ...)
    }

#endif
