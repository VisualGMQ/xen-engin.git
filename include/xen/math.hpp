#ifndef MATH_HPP
#define MATH_HPP

template <typename T>
struct Size {
    T w;
    T h;
};

using Sizei = Size<int>;
using Sizef = Size<float>;
using Sizeu = Size<unsigned int>;

#endif
