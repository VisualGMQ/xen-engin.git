#ifndef XENENGIN_IMGUI_IMGUI_LAYER_HPP
#define XENENGIN_IMGUI_IMGUI_LAYER_HPP

#include "pch.hpp"
#include "xen/layer.hpp"
#include "xen/event/window_event.hpp"
#include "xen/event/key_event.hpp"
#include "xen/event/mouse_event.hpp"

namespace Xen {

class ImGuiLayer: public Layer {
public:
    ImGuiLayer();
    ~ImGuiLayer();

    void OnAttach() override;
    void OnDetach() override;
    void OnImGuiRender() override;

    void Begin();
    void End();
};

}

#endif
