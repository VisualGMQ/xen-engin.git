#ifndef XEN_ENTRYPOINT_HPP
#define XEN_ENTRYPOINT_HPP

#include "xen/application.hpp"

extern Xen::Application* Xen::CreateApplication();

int main(int argc, char** argv);

#endif
