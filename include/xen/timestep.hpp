#ifndef XEN_TIMESTEP_HPP
#define XEN_TIMESTEP_HPP

namespace Xen {
class TimeStep {
public:
    TimeStep(float million_time = 0.0f):time_(million_time) {

    }

    inline float GetSecondTime() const { return time_/1000.0f; }
    inline float GetMillionTime() const { return time_; }

    operator float() { return GetSecondTime(); }

private:
    float time_;
};
}

#endif
