#type vertex
#version 330 core
layout (location = 0) in vec2 a_Position;
layout (location = 1) in vec2 a_TexCoord;

uniform mat4 project_view;
uniform mat4 model;

uniform float z_value;

out vec2 texcoord;

void main() {
    gl_Position = project_view * model * vec4(a_Position, z_value, 1.0);
    texcoord = a_TexCoord;
}

#type fragment
#version 330 core
layout (location = 0) out vec4 FragColor;

in vec2 texcoord;

uniform sampler2D tex;
uniform vec4 color;

void main() {
    FragColor = texture(tex, texcoord) * color;
}
