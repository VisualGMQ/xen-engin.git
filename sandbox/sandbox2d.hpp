#ifndef XENENGIN_SANDBOX_SANDBOX2D_HPP
#define XENENGIN_SANDBOX_SANDBOX2D_HPP

#include "xen/xen.hpp"

class SandBox2D: public Xen::Layer {
public:
    SandBox2D()
    :Xen::Layer("SandBox2D"), camera_controller_(1, 1)
    {
        wall_ = Xen::Texture2D::CreateColorTexture("sandbox/assets/textures/wall.jpg");
        icons_ = Xen::Texture2D::CreateColorTexture("sandbox/assets/textures/icons.png");
    }

    void OnUpdate(Xen::TimeStep ts) override {
        Xen::RenderCommand::SetClearColor(glm::vec4(0, 0.5, 0, 1));
        Xen::RenderCommand::Clear();

        camera_controller_.OnUpdate(ts);

        Xen::Renderer2D::BeginScene(camera_controller_.GetCamera());
        {
            Xen::Renderer2D::DrawTexture({0, 0}, {0.2, 0.2}, wall_, 0.5);
            Xen::Renderer2D::DrawRect({-0.4, 0}, {0.2, 0.5}, {1, 1, 0, 1}, 0);
            Xen::Renderer2D::DrawTexture({0.4, 0}, {0.1, 0.1}, wall_, 0.5);
            Xen::Renderer2D::DrawRect({0.2, 0}, {0.2, 0.5}, {1, 1, 0, 1}, 0);

            auto subtexture = Xen::SubTexture2D::CreateFromRect(icons_, {0, 0}, {90, 90});
            Xen::Renderer2D::DrawSubTexture({0, 0}, {0.1, 0.1}, subtexture, 0.7);
        }
        Xen::Renderer2D::EndScene();
    }

    void OnImGuiRender() override {
        static bool open = true;
        ImGui::Begin("SandBox2D", &open);
        ImGui::End();
    }

    void OnEvent(Xen::Event &event) override {
        camera_controller_.OnEvent(event);
    }

private:
    Xen::Ref<Xen::Texture2D> wall_;
    Xen::Ref<Xen::Texture2D> icons_;
    Xen::OrthoCameraController camera_controller_;
};

#endif
