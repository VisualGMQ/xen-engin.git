#ifndef TEST_SANDBOX_HPP
#define TEST_SANDBOX_HPP

#include "xen/xen.hpp"

#include "sandbox2d.hpp"
#include "sandbox3d.hpp"
#include "sandbox_ode.hpp"

class SandBox: public Xen::Application {
    public:
        SandBox() {
            Xen::Renderer::Init(Xen::RendererAPI::API::OpenGL);
            Xen::RenderCommand::SetViewport(0, 0, GetWindow().GetSize().w, GetWindow().GetSize().h);
            Xen::AudioMaster::Init(Xen::AudioAPI::API::SDL2_Mixer);
            PushOverlay(new SandBox3D);
        }

        ~SandBox() {
            Xen::Renderer::Shutdown();
            Xen::AudioMaster::Shutdown();
        }

    private:
};

Xen::Application* Xen::CreateApplication();

#endif
